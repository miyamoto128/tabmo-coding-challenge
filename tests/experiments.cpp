/*

Requires libboost_math and libboost_log installed

$ g++ -lboost_log -lpthread -DBOOST_LOG_DYN_LINK -Wall experiments.cpp -o experiments
$ ./experiments

*/

#include <boost/cstdfloat.hpp>
#include <boost/log/trivial.hpp>
#include <boost/multiprecision/cpp_dec_float.hpp>
#include <cmath>
#include <iostream>
#include <vector>


using namespace std;
using boost::multiprecision::cpp_dec_float_50;

const unsigned int INT_PRECISION = 100'000;


template<typename T>
void testFloatPrecisions(std::string typeName) {
	cout <<  endl << "using " << typeName << "(size " << sizeof(T) << ")," << endl << endl;

	vector<T> v = {12345.6789, 0.1478, 0.1475, 13.2, 3.15};
	for (auto d1: v) {
		T d2 = d1 * INT_PRECISION;
		cpp_dec_float_50 accurate(d2);
		unsigned long l1 = static_cast<unsigned long>(d2);
		unsigned long l2 = accurate.convert_to<unsigned long>();

		cout << "* " << typeName << " " << setprecision(10) << d1 << " x " << INT_PRECISION
			<< " = " << d2 << " to long -> " << l1 << " / " << l2 << endl;
	}
}

template<>
void testFloatPrecisions<cpp_dec_float_50>(std::string typeName) {
	cout <<  endl << "using " << typeName << "(size " << sizeof(cpp_dec_float_50) << ")," << endl << endl;

	vector<cpp_dec_float_50> v = {12345.6789, 0.1478, 0.1475, 13.2, 3.15};
	for (auto d1: v) {
		cpp_dec_float_50 d2(d1 * INT_PRECISION);
		unsigned long l = d2.convert_to<unsigned long>();

		cout << "* " << typeName << " " << setprecision(10) << d1 << " x " << INT_PRECISION
			<< " = " << d2 << " to long -> " << l << endl;
	}
}

struct Amount {
	static const unsigned int PRECISION = 10'000;

	long long value;

	Amount(long double amount): value(fromDouble(amount))	{}
	explicit operator double() const { return toDouble(value); }

	static long long fromDouble(long double amount) {
		return round(amount * PRECISION);
	}

	static long double toDouble(long long amount) {
		long double result = static_cast<long double>(amount);
		result /= PRECISION;
		return result;
	}
};

void testAmountFloatPrecisions(std::string typeName) {
	cout <<  endl << "using " << typeName << "(size " << sizeof(Amount) << ")," << endl << endl;

	vector<double> v = {12345.6789, 0.1478, 0.1475, 13.2, 3.15};
	for (auto d1: v) {
		Amount a(d1);
		unsigned long l = a.value;

		cout << "* " << typeName << " " << setprecision(10) << d1 << " x " << Amount::PRECISION
			<< " to long -> " << l << " to double -> " << static_cast<double>(a) 			<< endl;
	}
}

int main(int argc, char** argv) {
	cout << endl
		<< "sizeof" << endl
		<< "------" << endl
		<< endl
		<< "* size of short -> " << sizeof(short) << endl
		<< "* size of int -> " << sizeof(int) << endl
		<< "* size of long -> " << sizeof(long) << endl
		<< "* size of long long -> " << sizeof(unsigned long long) << endl
		<< "* size of unsigned long long -> " << sizeof(unsigned long long) << endl
		<< endl
		<< "* size of float -> " << sizeof(float) << endl
		<< "* size of double -> " << sizeof(double) << endl
		<< "* size of long double -> " << sizeof(long double) << endl;

	cout << endl
		<< "cast" << endl
		<< "----" << endl;

	testFloatPrecisions<float>("float");
	testFloatPrecisions<double>("double");
	testFloatPrecisions<long double>("long double");
	testFloatPrecisions<boost::float64_t>("float64_t");
	testFloatPrecisions<boost::float80_t>("float80_t");
	testFloatPrecisions<cpp_dec_float_50>("cpp_dec_float_50");
	testAmountFloatPrecisions("Amount");


	cout << endl
		<< "boost_log" << endl
		<< "---------" << endl;

	BOOST_LOG_TRIVIAL(trace) << "A trace severity message";
    BOOST_LOG_TRIVIAL(debug) << "A debug severity message";
    BOOST_LOG_TRIVIAL(info) << "An informational severity message";
    BOOST_LOG_TRIVIAL(warning) << "A warning severity message";
    BOOST_LOG_TRIVIAL(error) << "An error severity message";
    BOOST_LOG_TRIVIAL(fatal) << "A fatal severity message";


	cout << endl << "Done" << endl;
	return 0;
}
