#include <boost/test/unit_test.hpp>

#include <data/amount.h>

using namespace data;

BOOST_AUTO_TEST_SUITE( test_Amount_static_methods )

BOOST_AUTO_TEST_CASE( from_double )
{
	BOOST_TEST( 12345 == Amount::fromDouble(1.2345) );
    BOOST_TEST( 12345 == Amount::fromDouble(1.23456789) );
    BOOST_TEST( 123456789 == Amount::fromDouble(12345.6789) );

    BOOST_TEST( 500 == Amount::fromDouble(0.05) );
    BOOST_TEST( 31500 == Amount::fromDouble(3.15) );
    BOOST_TEST( 132000 == Amount::fromDouble(13.2) );
}

BOOST_AUTO_TEST_CASE( to_double )
{
	BOOST_TEST( 1.2345L == Amount::toDouble(12345) );
    BOOST_TEST( 12345.6789L == Amount::toDouble(123456789) );

    BOOST_TEST( 0.05L == Amount::toDouble(500) );
    BOOST_TEST( 3.15L == Amount::toDouble(31500) );
    BOOST_TEST( 13.2L == Amount::toDouble(132000) );
}

BOOST_AUTO_TEST_SUITE_END()


BOOST_AUTO_TEST_SUITE( test_Amount_instance )

BOOST_AUTO_TEST_CASE( create_from_double )
{
    Amount price(1.2345);
	BOOST_TEST( 12345 == price.value );

    Amount budget(12345.6789);
    BOOST_TEST( 123456789 == budget.value );

    budget.value -= 6789;
    BOOST_TEST( 123450000 == budget.value );
}

BOOST_AUTO_TEST_CASE( explicit_cast_to_double )
{
    Amount price(1.2345);
	BOOST_TEST( 1.2345L == static_cast<long double>(price) );

    Amount budget(12345.6789);
    BOOST_TEST( 12345.6789L == static_cast<long double>(budget) );
}

BOOST_AUTO_TEST_SUITE_END()
