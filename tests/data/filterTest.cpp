#include <boost/test/unit_test.hpp>

#include <data/filter.h>

using namespace data;


BOOST_AUTO_TEST_SUITE( test_Filter )

BOOST_AUTO_TEST_CASE( include_ifa )
{
    CommonFilter ifa(FilterType::include);
    BOOST_CHECK( !ifa.isFiltered("") );
    
    ifa.add("7c02cebb-c84f-4252-bde5-d112b8397363");
    ifa.add("e444b16d-ba3a-4b88-b3b7-a6a1f99bb7d2");
    ifa.add("CF78FC14-b1d3-4238-9804-7ec70236e7ec");
    BOOST_CHECK( ifa.isFiltered("") );

    BOOST_CHECK( ifa.isFiltered("unknown") );
    BOOST_CHECK( ifa.isFiltered("88888888-4444-4444-4444-cccccccccccc") );

    BOOST_CHECK( !ifa.isFiltered("7C02CEBB-c84f-4252-bde5-d112b8397363") );
    BOOST_CHECK( !ifa.isFiltered("e444b16d-ba3a-4b88-b3b7-a6a1f99bb7d2") );
    BOOST_CHECK( !ifa.isFiltered("cf78fc14-b1d3-4238-9804-7ec70236e7ec") );
}

BOOST_AUTO_TEST_CASE( exclude_lang )
{
    CommonFilter language(FilterType::exclude);
    BOOST_CHECK( !language.isFiltered("") );

    language.add("de");
    language.add("IT");
    BOOST_CHECK( language.isFiltered("") );

    BOOST_CHECK( language.isFiltered("DE") );
    BOOST_CHECK( language.isFiltered("it") );

    BOOST_CHECK( !language.isFiltered("EN") );
    BOOST_CHECK( !language.isFiltered("FR") );
}

BOOST_AUTO_TEST_CASE( include_app )
{
    ApplicationFilter application(FilterType::include);
    application.add("Puzzle");
    application.add("Kart");

    BOOST_CHECK( application.isFiltered("") );
    BOOST_CHECK( application.isFiltered("puzzle") );
    BOOST_CHECK( application.isFiltered(" My Puzz-le") );

    BOOST_CHECK( !application.isFiltered("Puzzle") );
    BOOST_CHECK( !application.isFiltered("Puzzles") );
    BOOST_CHECK( !application.isFiltered("Puzzle 2020") );
    BOOST_CHECK( !application.isFiltered("Original Puzzle") );
    BOOST_CHECK( !application.isFiltered("Best Puzzle 2020") );
}

BOOST_AUTO_TEST_CASE( exclude_app )
{
    ApplicationFilter application(FilterType::exclude);
    application.add("Puzzle");
    application.add("Kart");

    BOOST_CHECK( application.isFiltered("") );
    BOOST_CHECK( !application.isFiltered("puzzle") );
    BOOST_CHECK( !application.isFiltered(" My Puzz-le") );

    BOOST_CHECK( application.isFiltered("Puzzle") );
    BOOST_CHECK( application.isFiltered("Puzzles") );
    BOOST_CHECK( application.isFiltered("Puzzle 2020") );
    BOOST_CHECK( application.isFiltered("Original Puzzle") );
    BOOST_CHECK( application.isFiltered("Best Puzzle 2020") );
}


BOOST_AUTO_TEST_SUITE_END()
