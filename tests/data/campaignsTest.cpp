#include <boost/test/unit_test.hpp>

#include <data/campaigns.h>
#include <data/openrtb/bidrequest.h>

using namespace data;


struct CampaignFixture {

    Campaign campaign;
    openrtb::BidRequest bidreq;

    void setup() {
        campaign.id = "campaign_id";
        campaign.budget = 123456;
        campaign.bidPrice = 1234;
        campaign.width = 320;
        campaign.height = 250;
        campaign.responsive = false;
        campaign.url = "https://fake.sh";

        bidreq.device.w = 320;
        bidreq.device.h = 250;
    }
};

BOOST_FIXTURE_TEST_SUITE( test_single_Campaign, CampaignFixture )

BOOST_AUTO_TEST_CASE( creation )
{
    BOOST_TEST( "campaign_id" == campaign.id );
    BOOST_TEST( 123456 == campaign.budget.load() );
    BOOST_TEST( 1234 == campaign.bidPrice );
    BOOST_TEST( 320 == campaign.width );
    BOOST_TEST( 250 == campaign.height );
    BOOST_TEST( "https://fake.sh" == campaign.url );
}

BOOST_AUTO_TEST_CASE( copy_constructor )
{
    Campaign c2 = campaign;
    BOOST_TEST( "campaign_id" == c2.id );
    BOOST_TEST( 123456 == c2.budget.load() );
    BOOST_TEST( 1234 == c2.bidPrice );
    BOOST_TEST( 320 == c2.width );
    BOOST_TEST( 250 == c2.height );
    BOOST_TEST( "https://fake.sh" == c2.url );
}

BOOST_AUTO_TEST_CASE( bidrequest_size_compliance )
{
    BOOST_TEST( campaign.isSizeCompliant(320, 250) );
    BOOST_TEST( !campaign.isSizeCompliant(1, 250) );
    BOOST_TEST( !campaign.isSizeCompliant(320, 1) );

    BOOST_TEST( !campaign.isSizeCompliant(640, 500) );
    BOOST_TEST( !campaign.isSizeCompliant(32, 25) );
    BOOST_TEST( !campaign.isSizeCompliant(321, 250) );
    BOOST_TEST( !campaign.isSizeCompliant(320, 249) );

}

BOOST_AUTO_TEST_CASE( bidrequest_width_x_height_responsive_compliance )
{
    campaign.responsive = true;
    
    BOOST_TEST( campaign.isSizeCompliant(320, 250) );
    BOOST_TEST( !campaign.isSizeCompliant(1, 250) );
    BOOST_TEST( !campaign.isSizeCompliant(320, 1) );

    BOOST_TEST( campaign.isSizeCompliant(640, 500) );
    BOOST_TEST( campaign.isSizeCompliant(32, 25) );
    BOOST_TEST( !campaign.isSizeCompliant(321, 250) );
    BOOST_TEST( !campaign.isSizeCompliant(320, 249) );
}

BOOST_AUTO_TEST_CASE( filter_bidrequest_no_budget )
{
    BOOST_TEST( campaign.isSelectable(bidreq) );

    campaign.budget = 10000;
    campaign.bidPrice = 10000;
    BOOST_TEST( campaign.isSelectable(bidreq) );

    campaign.budget.fetch_sub(1);
    BOOST_TEST( !campaign.isSelectable(bidreq) );
}

BOOST_AUTO_TEST_CASE( bidrequest_selectable_size )
{
    BOOST_TEST( campaign.isSelectable(bidreq) );

    bidreq.device.w = 1;
    BOOST_TEST( !campaign.isSelectable(bidreq) );
}

BOOST_AUTO_TEST_CASE( bidrequest_selectable_lang )
{
    bidreq.device.lang = "IT";
    BOOST_TEST( campaign.isSelectable(bidreq) );

    CommonFilter filter(FilterType::exclude);
    campaign.language = filter;
    BOOST_TEST( campaign.isSelectable(bidreq) );

    campaign.language->add("IT");
    BOOST_TEST( !campaign.isSelectable(bidreq) );
}

BOOST_AUTO_TEST_CASE( bidrequest_selectable_app_name )
{
    bidreq.app.name = "Super Mario Kart";
    BOOST_TEST( campaign.isSelectable(bidreq) );

    ApplicationFilter filter(FilterType::include);
    campaign.application = filter;
    BOOST_TEST( !campaign.isSelectable(bidreq) );

    campaign.application->add("Kart");
    BOOST_TEST( campaign.isSelectable(bidreq) );
}

BOOST_AUTO_TEST_CASE( bidrequest_selectable_ifa )
{
    bidreq.device.ifa = "7C02CEBB-c84f-4252-bde5-d112b8397363";
    BOOST_TEST( campaign.isSelectable(bidreq) );

    CommonFilter filter(FilterType::include);
    campaign.ifa = filter;
    BOOST_TEST( !campaign.isSelectable(bidreq) );

    campaign.ifa->add("7C02CEBB-c84f-4252-bde5-d112b8397363");
    BOOST_TEST( campaign.isSelectable(bidreq) );
}

BOOST_AUTO_TEST_CASE( bidrequest_selectable_budget )
{
    BOOST_TEST( campaign.isSelectable(bidreq) );

    campaign.budget.store(campaign.bidPrice - 1);
    BOOST_TEST( !campaign.isSelectable(bidreq) );
}

BOOST_AUTO_TEST_CASE( spend_bid_price )
{
    campaign.budget = 10001;
    campaign.bidPrice = 10000;

    BOOST_TEST(campaign.spendBidPrice());
    BOOST_TEST(1 == campaign.budget.load());

    BOOST_TEST(!campaign.spendBidPrice());
    BOOST_TEST(1 == campaign.budget.load());
}

BOOST_AUTO_TEST_SUITE_END()


struct CampaignsFixture {

    Campaigns campaigns;

    void setup() {
        Campaign c1, c2, c3;

        c1.id = "id1";
        c1.budget = 1000;
        c1.bidPrice = 100;
        c1.url = "http://fake.io";

        c2.id = "id2";
        c2.budget = 200000;
        c2.bidPrice = 20000;
        c2.url = "http://win-notice.io";

        c3.id = "id3";
        c3.budget = 300000;
        c3.bidPrice = 30000;

        campaigns.add(c1);
        campaigns.add(c2);
        campaigns.add(c3);
    }
};

BOOST_FIXTURE_TEST_SUITE( test_multiple_Campaigns, CampaignsFixture )

BOOST_AUTO_TEST_CASE( check_created_campaigns )
{
	BOOST_TEST ( 3 == campaigns.size() );

    Campaign c;
    auto it = campaigns.begin();
    BOOST_TEST ( "id1" == it->get()->id );
    BOOST_TEST ( 1000 == it->get()->budget );
    BOOST_TEST ( 100 == it->get()->bidPrice );
    BOOST_TEST ( "http://fake.io" == it->get()->url );

    ++it;
    BOOST_TEST ( "id2" == it->get()->id );
    BOOST_TEST ( 200000 == it->get()->budget );
    BOOST_TEST ( 20000 == it->get()->bidPrice );
    BOOST_TEST ( "http://win-notice.io" == it->get()->url );

    ++it;
    BOOST_TEST ( "id3" == it->get()->id );
    BOOST_TEST ( 300000 == it->get()->budget );
    BOOST_TEST ( 30000 == it->get()->bidPrice );
    BOOST_TEST ( it->get()->url.empty() );
}

BOOST_AUTO_TEST_CASE( add_invalid_campaign )
{
    BOOST_TEST ( 3 == campaigns.size() );

    Campaign empty;
    BOOST_TEST ( !empty.validate() );
    BOOST_TEST ( !campaigns.add(empty) );

    BOOST_TEST ( 3 == campaigns.size() );
}

BOOST_AUTO_TEST_CASE( add_valid_campaign )
{
    BOOST_TEST ( 3 == campaigns.size() );

    Campaign c4;
    c4.id = "id4";
    BOOST_TEST ( c4.validate() );
    BOOST_TEST ( campaigns.add(c4) );

    BOOST_TEST ( 4 == campaigns.size() );
}

BOOST_AUTO_TEST_CASE( lower_bound_0 )
{
    auto it = campaigns.lowerBound(0);
    BOOST_TEST ( "id1" == it->get()->id );

    it = campaigns.lowerBound(0.01);
    BOOST_TEST ( "id1" == it->get()->id );

    it = campaigns.lowerBound(0.011);
    BOOST_TEST ( "id2" == it->get()->id );

    it = campaigns.lowerBound(2);
    BOOST_TEST ( "id2" == it->get()->id );

    it = campaigns.lowerBound(2.1);
    BOOST_TEST ( "id3" == it->get()->id );

    it = campaigns.lowerBound(3);
    BOOST_TEST ( "id3" == it->get()->id );
}

BOOST_AUTO_TEST_SUITE_END()
