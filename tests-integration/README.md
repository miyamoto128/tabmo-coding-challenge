Non-regression tests for Tabmo Coding Challenge.

## Usage

Get a local running *tabmo-coding-challenge*.

Then, execute all the tests:

```
pytest
``` 

## Getting start

This project uses poetry as dependency management and packaging tool.

To install poetry, follow the official guideline: https://python-poetry.org/docs/#installation.

Then, install the dependencies:

```
poetry install
```

Execute all the tests:

```
pytest
```
