import requests


def test_is_alive():
    r = requests.get("http://localhost:8090/api/v1/ping")
    assert r.status_code == 200

    resp = r.json()
    assert resp["message"] == "up"
