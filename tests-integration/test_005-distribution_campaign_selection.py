from collections import Counter
import requests

from constants import BIDREQUEST1, BIDREQUEST2, BIDREQUEST3

def test_distribution_campaigns():
    distribution = Counter()

    for _ in range(100):
        r = requests.post("http://localhost:8090/api/v1/rtb", json=BIDREQUEST1)
        assert r.status_code == 200
    
        resp = r.json()
        campaign_id = resp["campaignId"]
        distribution[campaign_id] += 1

    print("c1 -> c9c71e5a-4c24-493a-a7a0-436021af8252 (100$)")
    print("c2 -> a883b9a9-c6fb-49b0-8378-5ac5565779d1 (75$)")
    print("c3 -> a14b2507-ce69-4c38-87fb-cd8864051a30 (50$)")
    print("c4 -> 2674c919-ad0c-4d20-b461-456e82af74e2 (13.2$)")
    print(distribution)
    
    c1 = distribution["c9c71e5a-4c24-493a-a7a0-436021af8252"]   # Campaing with highest budget: 100$
    c2 = distribution["a883b9a9-c6fb-49b0-8378-5ac5565779d1"]   # 75$
    c3 = distribution["a14b2507-ce69-4c38-87fb-cd8864051a30"]   # 50$
    c4 = distribution["2674c919-ad0c-4d20-b461-456e82af74e2"]   # 13.2$

    assert c1 > c2
    assert c2 > c3
    assert c3 > c4
   