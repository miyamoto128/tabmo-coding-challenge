import copy
import requests

from constants import BIDREQUEST3


def test_bidrequest_rejected_by_size():
    bidreq = copy.deepcopy(BIDREQUEST3)

    assert_matching_bidrequest(bidreq)
    
    bidreq["device"]["w"] = 1
    r = requests.post("http://localhost:8090/api/v1/rtb", json=bidreq)
    assert r.status_code == 204

def test_bidrequest_rejected_by_floorprice():
    bidreq = copy.deepcopy(BIDREQUEST3)
    
    assert_matching_bidrequest(bidreq)
    
    bidreq["bid-floor"] = 1.4
    r = requests.post("http://localhost:8090/api/v1/rtb", json=bidreq)
    assert r.status_code == 204

def assert_matching_bidrequest(bidreq):
    r = requests.post("http://localhost:8090/api/v1/rtb", json=bidreq)
    assert r.status_code == 200

    resp = r.json()
    assert resp["auctionId"] == bidreq["id"]