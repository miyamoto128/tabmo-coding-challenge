import copy
import requests

from constants import BIDREQUEST1


def test_bidrequest_filter_by_language():
    bidreq = get_bidrequest()

    # Different language than expected by c9c71e5a-4c24-493a-a7a0-436021af8252
    bidreq["device"]["lang"] = "IT"  

    r = requests.post("http://localhost:8090/api/v1/rtb", json=bidreq)
    assert r.status_code == 200
    resp = r.json()
    assert resp["campaignId"] == "a14b2507-ce69-4c38-87fb-cd8864051a30"

def test_bidrequest_filter_by_application():
    bidreq = get_bidrequest()

    # Different application name than expected by c9c71e5a-4c24-493a-a7a0-436021af8252
    bidreq["app"]["name"] = "puzzle"

    r = requests.post("http://localhost:8090/api/v1/rtb", json=bidreq)
    assert r.status_code == 200
    resp = r.json()
    assert resp["campaignId"] == "a14b2507-ce69-4c38-87fb-cd8864051a30"

def test_bidrequest_filter_by_ifa():
    bidreq = get_bidrequest()

    # ifa rejected by c9c71e5a-4c24-493a-a7a0-436021af8252
    bidreq["device"]["ifa"] = "23C42E63-7C3F-4510-865F-B26353FB7583"

    r = requests.post("http://localhost:8090/api/v1/rtb", json=bidreq)
    assert r.status_code == 200
    resp = r.json()
    assert resp["campaignId"] == "a14b2507-ce69-4c38-87fb-cd8864051a30"

def get_bidrequest():
    """ 
    matches both campains c9c71e5a-4c24-493a-a7a0-436021af8252 and 
    a14b2507-ce69-4c38-87fb-cd8864051a30 both compliant to reponsive size.
    """
    bidreq = copy.deepcopy(BIDREQUEST1)
    bidreq["device"]["w"] = 640
    bidreq["device"]["h"] = 500
    return bidreq