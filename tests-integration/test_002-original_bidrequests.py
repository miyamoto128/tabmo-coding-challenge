import requests

from constants import BIDREQUEST1, BIDREQUEST2, BIDREQUEST3



def test_matching_bidrequest():
    r = requests.post("http://localhost:8090/api/v1/rtb", json=BIDREQUEST1)
    assert r.status_code == 200

    resp = r.json()
    assert resp["auctionId"] == "488d874f-927a-454b-aedc-2288809f6009"
    assert resp["campaignId"]
    assert resp["price"]
    assert resp["url"]

def test_bidrequest_with_no_match():
    r = requests.post("http://localhost:8090/api/v1/rtb", json=BIDREQUEST2)
    assert r.status_code == 204

def test_bidrequest_one_campaign():
    r = requests.post("http://localhost:8090/api/v1/rtb", json=BIDREQUEST3)
    assert r.status_code == 200

    resp = r.json()
    assert resp["auctionId"] == "488d874f-927a-454b-aedc-2288809f6009"
    assert resp["campaignId"] == "a14b2507-ce69-4c38-87fb-cd8864051a30"
    assert resp["price"] == 1.3
    assert resp["url"] == "http://cdn.tabmo.io/ads/2"
