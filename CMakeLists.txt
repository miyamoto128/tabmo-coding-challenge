cmake_minimum_required (VERSION 2.6)

project (tabmo-coding-challenge)

add_definitions(-std=c++17 -DBOOST_LOG_DYN_LINK)

add_subdirectory(third-party/drogon-1.0.0)

include_directories(${CMAKE_SOURCE_DIR}/src)

find_library(
	LIB_BOOST_LOG boost_log
	LIB_BOOST_TEST boost_unit_test_framework
)

# Main executable
add_executable(tabmo-coding-challenge
	src/main.cpp
	src/api/ping.cpp
	src/api/rtb.cpp
	src/app/bidder.cpp
	src/data/campaigns.cpp
	src/data/campaigns_mixer.cpp
	src/data/filter.cpp
)

target_link_libraries(tabmo-coding-challenge PRIVATE drogon
	${LIB_BOOST_LOG}
)

# Unit tests executable
add_executable(unit-tests
	src/data/campaigns.cpp
	src/data/filter.cpp
	tests/runner.cpp
	tests/data/amountTest.cpp
	tests/data/campaignsTest.cpp
	tests/data/filterTest.cpp
)

target_link_libraries(unit-tests PRIVATE drogon
	${LIB_BOOST_LOG}
	${LIB_BOOST_TEST}
)
