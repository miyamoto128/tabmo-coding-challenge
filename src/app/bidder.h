#pragma once

#include <optional>
#include <string>

#include <jsoncpp/json/value.h>

#include <data/campaigns.h>

namespace app
{
    class Bidder {
        static const size_t MAX_FILTER_LANGUAGE = 10;
        static const size_t MAX_FILTER_APPLICATION = 100;
        static const size_t MAX_FILTER_IFA = 100'000;

        data::Campaigns campaigns_;

    public:
        Bidder() = default;

        bool loadCampaigns(const std::string& path);
        std::optional<Json::Value> processBidRequest(const Json::Value* jsonBidRequest);

    private:
        static std::vector<std::string> jsonArrayToVector(const Json::Value& jsonArray, size_t limit);
        static data::Campaign jsonToCampaign(const Json::Value& jsonCampaign);
        static data::openrtb::BidRequest jsonToBidrequest(const Json::Value* jsonBidRequest);

    };
} // namespace app