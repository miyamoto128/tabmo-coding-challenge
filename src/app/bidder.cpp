#include "bidder.h"

#include <algorithm>
#include <fstream>

#include <boost/log/trivial.hpp>
#include <json/json.h>

#include <data/amount.h>
#include <data/campaigns.h>
#include <data/campaigns_mixer.h>
#include <data/openrtb/bidrequest.h>

using namespace std;
using namespace data;


data::Campaign jsonToCampaign(const Json::Value& jsonCampaign);
data::openrtb::BidRequest jsonToBidrequest(const Json::Value* jsonBidRequest);


namespace app {

    bool Bidder::loadCampaigns(const string& path) {
        BOOST_LOG_TRIVIAL(info) << "Parsing " << path << " ...";

        Json::Value root;
        std::ifstream config_doc(path, std::ifstream::binary);

        BOOST_LOG_TRIVIAL(debug) << "Opened " << path;

        config_doc >> root;
        for (int i = 0; i < root.size(); ++i) {
            BOOST_LOG_TRIVIAL(debug) << "Campaign #" << i;

            data::Campaign campaign = jsonToCampaign(root[i]);
            if (!campaigns_.add(campaign)) {
                return false;   // Stop at first invalid campaign
            }
        }
        return true;
    }

    optional<Json::Value> Bidder::processBidRequest(const Json::Value* jsonBidRequest) {
        data::openrtb::BidRequest bidrequest = jsonToBidrequest(jsonBidRequest);

        // Validate bidrequest
        if (bidrequest.id.empty()) {     // TODO improve the validation (consider all mandatory fields)
            BOOST_LOG_TRIVIAL(warning) << "Missing id";
            return {};
        }

        BOOST_LOG_TRIVIAL(info) << "Received bidrequest " << bidrequest.id;

        vector<CampaignPtr> selectableCampaigns;
        copy_if(
            campaigns_.lowerBound(bidrequest.bidfloor),
            campaigns_.end(),
            back_inserter(selectableCampaigns),
            [&bidrequest] (CampaignPtr c) { return c->isSelectable(bidrequest); }
        );

        if (selectableCampaigns.empty()) {
            BOOST_LOG_TRIVIAL(info) << "No selectable campaign";
            return {};
        }

        // Shuffle selectable campaigns
        CampaignsMixer mixer(selectableCampaigns);
        selectableCampaigns = mixer.weighedShuffle();

        BOOST_LOG_TRIVIAL(info) << "Selectable campaigns:";
        for (CampaignPtr campaign : selectableCampaigns) {
            BOOST_LOG_TRIVIAL(info) << "* Campaign " << campaign->id
                << ", " << campaign->budget
                << ", " << campaign->bidPrice;
        }

        // Cycle selectable campaigns until one has the budget to bid
        CampaignPtr selectedCampaign;
        for (CampaignPtr campaign : selectableCampaigns) {
            if (!campaign->spendBidPrice()) {
                BOOST_LOG_TRIVIAL(warning) << "Not enough budget for campaign " << campaign->id;
            } else {
                selectedCampaign = campaign;
                break;
            }
        }

        if (!selectedCampaign) {
            BOOST_LOG_TRIVIAL(warning) << "No selectable campaign with enough budget";
            return {};
        }

        BOOST_LOG_TRIVIAL(info) << "Selected Campaign " << selectedCampaign->id
            << ", budget left: " << Amount::toDouble(selectedCampaign->budget.load()) << "$";

        // Compose JSON bid response
        Json::Value ret;
        ret["auctionId"] = bidrequest.id;
        ret["campaignId"] = selectedCampaign->id;
        ret["url"] = selectedCampaign->url;
        ret["price"] = static_cast<double>(Amount::toDouble(selectedCampaign->bidPrice));

        return optional<Json::Value>{ret};
    }

    vector<string> Bidder::jsonArrayToVector(const Json::Value& jsonArray, size_t limit) {
        vector<string> result;

        if (jsonArray.size() < limit) {
            limit = jsonArray.size();
        }

        for (int i = 0; i < limit; ++i) {
            string element = jsonArray[i].asString();
            result.push_back(element);
        }
        return result;
    }

    data::Campaign Bidder::jsonToCampaign(const Json::Value& jsonCampaign) {
        data::Campaign campaign;
        campaign.id = jsonCampaign.get("id", "").asString();
        campaign.budget = Amount::fromDouble(stod(jsonCampaign.get("budget", "0").asString()));
        campaign.bidPrice = Amount::fromDouble(stod(jsonCampaign.get("bidPrice", "0").asString()));
        campaign.width = jsonCampaign.get("width", 0).asUInt();
        campaign.height = jsonCampaign.get("height", 0).asUInt();
        campaign.responsive = jsonCampaign.get("responsive", false).asBool();
        campaign.url = jsonCampaign.get("url", "").asString();

        // filters
        const Json::Value includeLang = jsonCampaign["filters"]["include"]["language"];
        const Json::Value excludeLang = jsonCampaign["filters"]["exclude"]["language"];
        const Json::Value includeApp = jsonCampaign["filters"]["include"]["application"];
        const Json::Value excludeApp = jsonCampaign["filters"]["exclude"]["application"];
        const Json::Value includeIfa = jsonCampaign["filters"]["include"]["ifa"];
        const Json::Value excludeIfa = jsonCampaign["filters"]["exclude"]["ifa"];

        if (!includeLang.empty()) {
            campaign.language = CommonFilter(FilterType::include);
            campaign.language->add(jsonArrayToVector(includeLang, MAX_FILTER_LANGUAGE));
        } else if (!excludeLang.empty()) {
            campaign.language = CommonFilter(FilterType::exclude);
            campaign.language->add(jsonArrayToVector(excludeLang, MAX_FILTER_LANGUAGE));
        }

        if (!includeApp.empty()) {
            campaign.application = ApplicationFilter(FilterType::include);
            campaign.application->add(jsonArrayToVector(includeApp, MAX_FILTER_APPLICATION));
        } else if (!excludeApp.empty()) {
            campaign.application = ApplicationFilter(FilterType::exclude);
            campaign.application->add(jsonArrayToVector(excludeApp, MAX_FILTER_APPLICATION));
        }

        if (!includeIfa.empty()) {
            campaign.ifa = CommonFilter(FilterType::include);
            campaign.ifa->add(jsonArrayToVector(includeIfa, MAX_FILTER_IFA));
        } else if (!excludeIfa.empty()) {
            campaign.ifa = CommonFilter(FilterType::exclude);
            campaign.ifa->add(jsonArrayToVector(excludeIfa, MAX_FILTER_IFA));
        }

        BOOST_LOG_TRIVIAL(trace) << "Parsed " << campaign.id
            << ", " << campaign.budget
            << ", " << campaign.bidPrice
            << ", " << campaign.width
            << "x" << campaign.height
            << ", " << campaign.responsive
            << ", " << campaign.url;

        if (campaign.language) {
            BOOST_LOG_TRIVIAL(trace) << " * lang: " << campaign.language->toString();
        }
        if (campaign.application) {
            BOOST_LOG_TRIVIAL(trace) << " * lang: " << campaign.application->toString();
        }
        if (campaign.ifa) {
            BOOST_LOG_TRIVIAL(trace) << " * lang: " << campaign.ifa->toString();
        }

        return campaign;
    }

    data::openrtb::BidRequest Bidder::jsonToBidrequest(const Json::Value* jsonBidRequest) {
        data::openrtb::BidRequest bidrequest;
        bidrequest.id = jsonBidRequest->get("id", "").asString();
        bidrequest.bidfloor = stod(jsonBidRequest->get("bid-floor", "").asString());
        bidrequest.device.ifa = (*jsonBidRequest)["device"].get("ifa", "").asString();
        bidrequest.device.lang = (*jsonBidRequest)["device"].get("lang", "").asString();
        bidrequest.device.w = (*jsonBidRequest)["device"].get("w", 0).asUInt();
        bidrequest.device.h = (*jsonBidRequest)["device"].get("h", 0).asUInt();
        bidrequest.app.name = (*jsonBidRequest)["app"].get("name", "").asString();

        BOOST_LOG_TRIVIAL(trace) << "Bidrequest " << bidrequest.id
            << ", " << (long double) bidrequest.bidfloor
            << "$, " << bidrequest.device.ifa
            << ", " << bidrequest.device.lang
            << ", " << bidrequest.device.w
            << "x" << bidrequest.device.h
            << ", " << bidrequest.app.name;

        return bidrequest;
    }

} // app
 