#pragma once

#include <set>
#include <string>
#include <vector>


namespace data {

    enum FilterType { include = 0, exclude };

    class AbstractFilter {
    protected:
        FilterType type_;
        std::set<std::string> elements_;

    public:
        AbstractFilter(FilterType type);

        // Accessors
        std::string type() const;

        void add(std::string element);
        void add(const std::vector<std::string> elements);
        bool isFiltered(const std::string& input) const;

        std::string toString() const;

    protected:
        virtual void doAdd(std::string element);
        virtual bool isIncludedFiltered(const std::string& input) const = 0;
        virtual bool isExcludedFiltered(const std::string& input) const = 0;


    };

    class CommonFilter: public AbstractFilter {
    
    public:
        CommonFilter(FilterType type);

        void doAdd(std::string element);
        bool isIncludedFiltered(const std::string& input) const;
        bool isExcludedFiltered(const std::string& input) const;
    };

    // Specific to app.name: keep the original case, and check if an input is contained rather than equals.
    class ApplicationFilter: public AbstractFilter {
    public:
        ApplicationFilter(FilterType type);

        bool isIncludedFiltered(const std::string& input) const;
        bool isExcludedFiltered(const std::string& input) const;
    };

} // data