#pragma once

#include <atomic>
#include <memory>
#include <mutex>
#include <set>
#include <string>
#include <vector>

#include <data/amount.h>
#include <data/filter.h>
#include <data/openrtb/bidrequest.h>

namespace data {

    class Campaign {
        std::mutex budgetMutex_;

    public:
        std::string id;
        std::atomic_ullong budget;
        unsigned long long bidPrice;
        unsigned int width;
        unsigned int height;
        bool responsive;
        std::optional<CommonFilter> language;
        std::optional<ApplicationFilter> application;
        std::optional<CommonFilter> ifa;
        std::string url;

        Campaign() = default;
        Campaign(const Campaign& other);
        Campaign& operator=(const Campaign& other) = delete;


        bool validate() const;
        bool isSelectable(const openrtb::BidRequest& bidrequest) const;
        bool isSizeCompliant(unsigned int w, unsigned int h) const;
        bool spendBidPrice();
    };


    typedef std::shared_ptr<Campaign> CampaignPtr;

    struct cmp {
        bool operator() (const CampaignPtr lhs, const CampaignPtr rhs) const {
            return lhs->bidPrice < rhs->bidPrice;
        }
    };

    typedef std::multiset<CampaignPtr, cmp> CampaignsByBid;



    class Campaigns {
        CampaignsByBid orderedCampaigns_;

    public:
        Campaigns() = default;

        bool add(Campaign campaign);

        CampaignsByBid::iterator lowerBound(Amount bidfloor);
        CampaignsByBid::iterator begin();
        CampaignsByBid::iterator end();
        size_t size() const;
    };

} // data