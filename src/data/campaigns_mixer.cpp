#include "campaigns_mixer.h"

#include <boost/log/trivial.hpp>

using namespace std;

namespace data {

    CampaignsMixer::CampaignsMixer(const vector<CampaignPtr>& campaigns):
        count_(campaigns.size()) 
    {
        for (size_t i = 0; i < campaigns.size(); ++i) {
            CampaignPtr campaign = campaigns.at(i);
            indexedCampaigns_[i] = campaign;
            indexedBudgets_[i] = campaign->budget.load();
        }
    }

    vector<CampaignPtr> CampaignsMixer::weighedShuffle() {
        vector<CampaignPtr> result;

        random_device rd;
        mt19937 gen(rd());

        while (result.size() < count_) {
            vector<unsigned long long> weights = getBudgets();

            discrete_distribution<> distribution(weights.cbegin(), weights.cend());
            size_t offset = distribution(gen);

            auto it = indexedBudgets_.begin();
            auto nx = next(it, offset);
            size_t index = nx->first;

            BOOST_LOG_TRIVIAL(trace) << "Offset: " << offset 
                << " -> Index: " << index 
                << " -> Campaign: " << indexedCampaigns_[index]->id
                << ", budget: " << nx->second;

            result.push_back(indexedCampaigns_[index]);
            indexedBudgets_.erase(nx);
        }
        return result;
    }

    vector<unsigned long long> CampaignsMixer::getBudgets() const {
        vector<unsigned long long> budgets;

        for (pair<size_t, unsigned long long> entry: indexedBudgets_) {
            BOOST_LOG_TRIVIAL(trace) << "* Budget [" << entry.first << "] = " << entry.second;
            budgets.push_back(entry.second);
        }

        return budgets;
    }

} // data



