#pragma once

#include <cmath>

namespace data {

    class Amount {
        static const unsigned int PRECISION = 10'000;


    public:
        long long value;

        Amount() = default;
        Amount(long double amount): value(fromDouble(amount))	{}

        explicit operator long double() const { return toDouble(value); }

        static long long fromDouble(long double amount) {
            return llroundl(amount * PRECISION * 10) / 10;
        }

        static long double toDouble(long long amount) {
            long double result = static_cast<long double>(amount);
            result /= PRECISION;
            return result;
        }
    };
}