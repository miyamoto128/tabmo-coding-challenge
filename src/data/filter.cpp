#include "filter.h"

#include <boost/algorithm/string.hpp>
#include <boost/log/trivial.hpp>

#include <sstream>

using namespace std;

namespace data {

    //
    // AbstractFilter
    //

    AbstractFilter::AbstractFilter(FilterType type):
        type_(type)
    {}

    std::string AbstractFilter::type() const {
        if (type_ == 0) {
            return "include";
        }
        return "exclude";
    }

    void AbstractFilter::add(string element) {
        doAdd(element);
    }

    void AbstractFilter::add(vector<string> elements) {
        for (string element: elements) {
            doAdd(element);
        }
    }

    bool AbstractFilter::isFiltered(const string& input) const {
        // If no input then: if filter available return filtered, false otherwise
        if (input.empty()) {
            return !elements_.empty();
        }

        // Apply filter
        bool result = false;
        switch(type_) {
            case include:
                result = isIncludedFiltered(input);
                break;
            case exclude:
                result = isExcludedFiltered(input);
                break;
        }
        
        if (result) {
            BOOST_LOG_TRIVIAL(debug) << input << " filtered";
            BOOST_LOG_TRIVIAL(trace) << " by " << toString();
        }

        return result;
    }

    string AbstractFilter::toString() const {
        ostringstream oss;
        oss << type() << " [ ";
        for (string element: elements_) {
            oss << element << ", ";
        }
        oss << "] (" << elements_.size() << ")";
        return oss.str();
    }

    void AbstractFilter::doAdd(string element) {
        elements_.insert(element);
    }

    //
    // CommonFilter
    //

    CommonFilter::CommonFilter(FilterType type):
        AbstractFilter(type)
    {}
    
    void CommonFilter::doAdd(string element) {
        boost::algorithm::to_lower(element);
        elements_.insert(element);
    }

    bool CommonFilter::isIncludedFiltered(const string& input) const {
        string lowerCaseInput = boost::algorithm::to_lower_copy(input);
        return elements_.find(lowerCaseInput) == elements_.cend();
    }

    bool CommonFilter::isExcludedFiltered(const string& input) const {
        return !isIncludedFiltered(input);
    }

    //
    // ApplicationFilter
    //

    ApplicationFilter::ApplicationFilter(FilterType type):
        AbstractFilter(type)
    {}

    bool ApplicationFilter::isIncludedFiltered(const string& input) const {
        for (string element: elements_) {
            if (input.find(element) != string::npos) {
                return false;
            }
        }
        return true;
    }

    bool ApplicationFilter::isExcludedFiltered(const string& input) const {
        for (string element: elements_) {
            if (input.find(element) != string::npos) {
                return true;
            }
        }
        return false;
    }
    
} // data