#include "campaigns.h"

#include <boost/log/trivial.hpp>

namespace data {

    //
    // Single Campaign
    //

    Campaign::Campaign(const Campaign& other) :
        id(other.id),
        budget(other.budget.load()),
        bidPrice(other.bidPrice),
        width(other.width),
        height(other.height),
        responsive(other.responsive),
        language(other.language),
        application(other.application),
        ifa(other.ifa),
        url(other.url)
    {}


    bool Campaign::spendBidPrice() {
        bool success = false;

        budgetMutex_.lock();
        if (budget.load() >= bidPrice) {
            budget.fetch_sub(bidPrice);
            success = true;
        }
        budgetMutex_.unlock();

        return success;
    }

    bool Campaign::validate() const {
        // TODO improve the validation (consider all mandatory fields)
        return !id.empty();
    }

    bool Campaign::isSelectable(const openrtb::BidRequest& bidrequest) const {
        BOOST_LOG_TRIVIAL(debug) << "Campaign " << id << "|bidrequest " << bidrequest.id << " compliance,";

        if (!isSizeCompliant(bidrequest.device.w, bidrequest.device.h)) {
            BOOST_LOG_TRIVIAL(debug) << "Non compliant size";
        } else if (language && language->isFiltered(bidrequest.device.lang)) {
            BOOST_LOG_TRIVIAL(debug) << "Filtered on language (" << language->type() << ")";
        } else if (application && application->isFiltered(bidrequest.app.name)) {
            BOOST_LOG_TRIVIAL(debug) << "Filtered on application (" << application->type() << ")";
        } else if (ifa && ifa->isFiltered(bidrequest.device.ifa)) {
            BOOST_LOG_TRIVIAL(debug) << "Filtered on ifa (" << ifa->type() << ")";
        } else if (budget.load() < bidPrice) {
            BOOST_LOG_TRIVIAL(debug) << "Not enough budget";
        } else {
            BOOST_LOG_TRIVIAL(debug) << "-> selectable";
            return true;
        }

        BOOST_LOG_TRIVIAL(debug) << "-> skipped";
        return false;
    }

    bool Campaign::isSizeCompliant(unsigned int w, unsigned int h) const {
        if (responsive) {
            // Compare ratio using multiplications (and avoid inexact divisions)
            return width * h == w * height;
        }
        return width == w && height == h;
    }


    //
    // Multiple Campaigns
    //

    bool Campaigns::add(Campaign campaign) {
        if (!campaign.validate()) {
            BOOST_LOG_TRIVIAL(debug) << "Invalid campaign, no add";
            return false;
        }

        orderedCampaigns_.insert(std::make_shared<Campaign>(campaign));
        return true;
    }

    CampaignsByBid::iterator Campaigns::lowerBound(Amount bidfloor) {
        Campaign lower;
        lower.bidPrice = bidfloor.value;

        return orderedCampaigns_.lower_bound(std::make_shared<Campaign>(lower));
    }

    CampaignsByBid::iterator Campaigns::begin() {
        return orderedCampaigns_.begin();
    }

    CampaignsByBid::iterator Campaigns::end() {
        return orderedCampaigns_.end();
    }

    size_t Campaigns::size() const {
        return orderedCampaigns_.size();
    }
    
} // data