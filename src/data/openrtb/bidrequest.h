#pragma once

#include <string>

#include <data/amount.h>

namespace data::openrtb {

    struct Device {
        std::string ifa;
        std::string lang;
        unsigned int w;
        unsigned int h;
    };

    struct App {
        std::string name;
    };

    struct BidRequest {
        std::string id;
        Device device;
        App app;
        Amount bidfloor;
    };

} // data::openrtb