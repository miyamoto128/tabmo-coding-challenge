#pragma once

#include<map>
#include<random>
#include<vector>

#include <boost/log/trivial.hpp>

#include <data/campaigns.h>

namespace data {

    class CampaignsMixer {
        size_t count_;
        std::map<size_t, CampaignPtr> indexedCampaigns_;
        std::map<size_t, unsigned long long> indexedBudgets_;
        
    public:
        CampaignsMixer(const std::vector<CampaignPtr>& campaigns);
        std::vector<CampaignPtr> weighedShuffle();

    private:
        std::vector<unsigned long long> getBudgets() const;
    };

} // namespace data