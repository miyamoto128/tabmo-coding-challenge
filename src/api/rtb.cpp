#include "rtb.h"

using namespace std;
using namespace drogon;

namespace api {

    HttpResponsePtr newEmptyResponse() {
        auto resp = HttpResponse::newHttpResponse();
        resp->setStatusCode(k204NoContent);
        return resp;
    }

    namespace v1 {
        void Rtb::post(const drogon::HttpRequestPtr &req, std::function<void(const drogon::HttpResponsePtr &)> &&callback) {
            auto resp = newEmptyResponse(); // Default, empty response

            auto jsonObject = req->getJsonObject();

            if (jsonObject) {
                auto result = bidder_.processBidRequest(jsonObject.get());

                if (result) {
                    Json::Value ret = result.value();
                    resp = HttpResponse::newHttpJsonResponse(ret);
                }
            }
            callback(resp);
        }
    }
}