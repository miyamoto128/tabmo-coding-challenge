#pragma once

#include <drogon/HttpController.h>

namespace api
{
    namespace v1
    {
        class Ping : public drogon::HttpController<Ping>
        {
        public:
            Ping() = default;

            METHOD_LIST_BEGIN
                METHOD_ADD(Ping::get, "", drogon::Get); //path is /api/v1/ping
            METHOD_LIST_END

            void get(const drogon::HttpRequestPtr &req, std::function<void(const drogon::HttpResponsePtr &)> &&callback) const;
        };
    } // namespace v1
} // namespace api