#pragma once

#include <drogon/HttpController.h>

#include <app/bidder.h>

namespace api
{
    namespace v1
    {
        class Rtb : public drogon::HttpController<Rtb, false>
        {
            app::Bidder bidder_;

        public:
            explicit Rtb(app::Bidder& bidder) : bidder_(bidder) {}

            METHOD_LIST_BEGIN
                METHOD_ADD(Rtb::post, "", drogon::Post); //path is /api/v1/rtb
            METHOD_LIST_END

            void post(const drogon::HttpRequestPtr &req, std::function<void(const drogon::HttpResponsePtr &)> &&callback);
        };
    } // namespace v1
} // namespace api