#include "ping.h"

using namespace drogon;

namespace api {
    namespace v1 {
        void Ping::get(const drogon::HttpRequestPtr &req, std::function<void(const drogon::HttpResponsePtr &)> &&callback) const {
            Json::Value ret;
            ret["message"] = "up";
            auto resp = HttpResponse::newHttpJsonResponse(ret);
            callback(resp);
        }
    }
}