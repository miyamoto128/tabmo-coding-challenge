#include <api/ping.h>
#include <api/rtb.h>
#include <app/bidder.h>

#include <iostream>
#include <string>


void usage(std::string command, std::string error) {
    using namespace std;

    cerr << endl << error << endl
         << endl << "Usage:"
         << endl << command << " <campaigns>" << endl << endl;
}

int main(int argc, char **argv)
{
    // Check parameters
    if (argc < 2) {
        usage(argv[0], "Missing campaigns file!");
        return 1;
    }

    // Load campaigns data
    std::string path = argv[1];

    app::Bidder bidder;
    if (!bidder.loadCampaigns(path)) {
        LOG_FATAL << "Can not load campaigns";
        return 2;
    }

    // Start webserver
    auto rtbPtr = std::make_shared<api::v1::Rtb>(bidder);

    LOG_INFO << "Starting server on port 8090";

    drogon::app().setLogPath("./")
            .setLogLevel(trantor::Logger::kNumberOfLogLevels)
            .addListener("0.0.0.0", 8090)
            .setThreadNum(8)
            .registerController(rtbPtr)
            .run();

    return 0;
}

